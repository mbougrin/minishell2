##
## Makefile for Makefile in /home/sauvag_c/rendu/PSU/PSU_2015_minishell2
## 
## Made by clément sauvage
## Login   <sauvag_c@epitech.net>
## 
## Started on  Wed Mar 23 16:38:25 2016 clément sauvage
## Last update Wed Mar 23 16:38:29 2016 clément sauvage
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -I./include

NAME	= mysh

SRC	= src/tool.c \
	  src/tool2.c \
	  src/tool3.c \
	  src/tool4.c \
	  src/tool5.c \
	  src/getnextline.c \
	  src/exec.c \
	  src/builtin.c \
	  src/env.c \
	  src/cd.c \
	  src/main.c \
	  src/error.c \
	  src/cd_err.c

OBJ	= $(SRC:.c=.o)

all	: $(NAME)

$(NAME)	: $(OBJ)
	$(CC) -o $(NAME) $(OBJ)

clean	:
	$(RM) $(OBJ)

fclean	: clean
	$(RM) $(NAME)

re	: fclean all

.PHONY	: all clean fclean re

/*
** main.h for main in /home/sauvag_c/rendu/PSU/PSU_2015_minishell2/include
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Wed Mar 23 16:38:03 2016 clément sauvage
** Last update Wed Mar 23 16:38:06 2016 clément sauvage
*/

#ifndef MAIN_H_
# define MAIN_H_

# define BUFF_SIZE 128

#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>

char	*my_cmd(char *s1, char *s2);
char	**my_strstrnew(size_t size);
char	**my_strstrdup(char **s);
char	**my_str_to_word_tab(char const *s, char c);
char	*my_strcat(char *s1, const char *s2);
char	*my_strcpy(char *s1, char *s2);
char	*my_strchr(const char *s, int c);
char	*my_strjoin(char const *s1, char const *s2);
char	**my_envp(char **str);
char	*my_getenv(char *str);
char	*my_strnew(size_t size);
char	*my_strsub(char const *s, unsigned int start, size_t len);
char	*my_strdup(char *s1);
char	*my_strstr(const char *s1, const char *s2);
char	**my_strstrnew(size_t size);
void	 my_putstr_fd(char const *s, int fd);
void	my_error(char c);
void	my_puterror(const char *str);
void	my_execve(char *cmd, char **arg);
void	my_env();
void	my_signal();
void	my_strstrdel(char **s);
void	my_unsetenv(char *line);
void	my_putstr(const char *str);
void	my_setenv_replace(char **tab);
void	my_setenv_add(char **tab);
void	my_setenv(char *line);
void	my_change_directory(char **tab);
void	my_strdel(char **as);
void	my_cd_home();
void	my_cd(char *path);
void	my_launch_binary(char *line);
void	my_bzero(void *s, size_t n);
void	my_putendl(char const *s);
void	my_putchar(char c);
void	my_putendl_fd(char const *s, int fd);
int	my_strncmp(const char *s1, const char *s2, size_t n);
int	my_strlen(const char *str);
int	my_strcmp(const char *s1, const char *s2);
int	get_next_line(int fd, char **line);
int	my_strstrlen(char **s);
int	my_check_del(char **tab, char **env);
int	my_cd_check(char *s);

#endif /* !MAIN_H*/

/*
** exec.c for handler in /home/sauvag_c/rendu/PSU/PSU_2015_minishell2
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Wed Mar 23 16:40:03 2016 clément sauvage
** Last update Thu Apr  7 16:38:42 2016 clément sauvage
*/

#include "main.h"

void	my_handler(int nb)
{
  if (nb == SIGINT)
    {
      my_putchar('\n');
      my_putstr("$> ");
    }
}

void	my_signal()
{
  signal(SIGINT, my_handler);
  signal(SIGTSTP, SIG_IGN);
}

char		**my_envp(char **str)
{
  static	char**env;

  if (str != NULL)
    env = str;
  return (env);
}


void	my_execve(char *cmd, char **arg)
{
  pid_t	pid;

  if ((pid = fork()) < 0)
    {
      my_putendl("fork error");
      return;
    }
  signal(SIGINT, SIG_DFL);
  if (pid == 0)
    execve(cmd, arg, my_envp(NULL));
  else
    wait(NULL);
  my_signal();
}

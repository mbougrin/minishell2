/*
** tool4.c for tool4 in /home/sauvag_c/rendu/PSU/PSU_2015_minishell1/src
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Sat Jan 23 18:22:17 2016 clément sauvage
** Last update Thu Apr  7 22:08:08 2016 clément sauvage
*/

#include "main.h"

char	*my_strcat(char *s1, const char *s2)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (s1[i] != '\0')
    i++;
  while (s2[j] != '\0')
    {
      s1[i] = s2[j];
      i++;
      j++;
    }
  s1[i] = '\0';
  return (s1);
}

static int	my_next(const char *str, char charset, int i)
{
  int	first;
  int	result;

  result = 0;
  first = i;
  while (str[i] != '\0')
    {
      if (str[i] == charset)
	break;
      i++;
    }
  result = i - first;
  return (result);
}

static int	my_charset(const char *str, char charset)
{
  int		i;
  int		result;

  i = 0;
  result = 1;
  while (str[i] != '\0')
    {
      if (str[i] == charset)
	{
	  result++;
	  while (str[i] == charset)
	    i++;
	}
      i++;
    }
  return (result);
}

char	**my_str_to_word_tab(char const *s, char c)
{
  char	**split;
  int	i;
  int	n;

  i = 0;
  n = 0;
  split = (char **)malloc(sizeof(char *) * (my_charset(s, c) + 1));
  while (s[i] != '\0')
    {
      while (s[i] == c)
	i++;
      if (s[i] == '\0')
	break ;
      split[n] = my_strsub(s, i, my_next(s, c, i));
      i += my_next(s, c, i);
      n++;
    }
  split[n] = NULL;
  return (split);
}

void	my_strstrdel(char **s)
{
  int	i;

  i = 0;
  while (s[i] != NULL)
    {
      free(s[i]);
      s[i] = NULL;
      i++;
    }
  free(s);
  s = NULL;
}

/*
** builtin.c for builtin in /home/sauvag_c/rendu/PSU/PSU_2015_minishell2
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Wed Mar 23 16:38:58 2016 clément sauvage
** Last update Thu Apr  7 22:04:16 2016 clément sauvage
*/

#include "main.h"

void	my_env()
{
  char	**env;
  int	i;

  i = 0;
  env = my_envp(NULL);
  if (env == NULL)
    return ;
  while (env[i])
    {
      my_putendl(env[i]);
      i++;
    }
}

char	*my_getenv(char *str)
{
  int	len;
  char	**env;
  int	i;

  i = 0;
  env = my_envp(NULL);
  len = my_strlen(str);
  while (env[i])
    {
      if (my_strncmp(env[i], str, len) == 0)
	{
	  return (my_strdup(&env[i][len + 1]));
	}
      i++;
    }
  return (NULL);
}

char	*my_join(char *s1, char *s2)
{
  char	*ret;
  int	len;

  len = my_strlen(s1) + my_strlen(s2) + 1;
  ret = (char *)malloc(sizeof(char) * len);
  my_strcpy(ret, s1);
  my_strcat(ret, "/");
  my_strcat(ret, s2);
  return (ret);
}

void		my_dir(char **av, char **tab)
{
  DIR		*dir;
  struct dirent	*play;
  int		i;
  char		*cmd;
  
  i = 0;
  while (tab[i])
    {
      dir = opendir(tab[i]);
      if (dir != NULL)
	{
	  while ((play = readdir(dir)))
	    {
	      if (my_strcmp(play->d_name, av[0]) == 0)
		{
		  cmd = my_join(tab[i], play->d_name);
		  my_execve(cmd, av);
		  my_strdel(&cmd);
		  return;
		}
	    }
	  closedir(dir);
	}
      i++;
    }
  my_putendl("command not found");
}

void	my_launch_binary(char *line)
{
  char	*path;
  char	**tab;
  char	**av;

  av = my_str_to_word_tab(line, ' ');
  path = my_getenv("PATH");
  tab = my_str_to_word_tab(path, ':');
  my_dir(av, tab);
  my_strstrdel(tab);
  my_strstrdel(av);
  my_strdel(&path);
}

char	*my_strstr(const char *s1, const char *s2)
{
  if (*s2 == '\0')
    return ((char *)s1);
  while (my_strncmp(s1, s2, my_strlen(s2)) != 0)
    {
      if (*s1 == '\0')
	return (NULL);
      s1++;
    }
  return ((char*)s1);
}

int	my_check_del(char **tab, char **env)
{
  int	len;
  int	i;
  int	j;
  
  len = my_strstrlen(env);
  i = 0;
  while (env[i])
    {
      j = 1;
      while (tab[j])
	{
	  if (my_strstr(env[i], tab[j]) != NULL)
	    len--;
	  j++;
	}
      i++;
    }
  return (len);
}

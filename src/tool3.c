/*
** tool3.c for tool3 in /home/sauvag_c/rendu/PSU/PSU_2015_minishell1/src
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Sat Jan 23 18:17:49 2016 clément sauvage
** Last update Sun Jan 24 12:13:26 2016 clément sauvage
*/

#include "main.h"
#include <string.h>
#include <stdlib.h>

char		**my_strstrnew(size_t size)
{
  size_t	i;
  char		**s;

  i = 0;
  s = (char **)malloc(sizeof(char *) * (size + 1));
  if (s == NULL)
    return (NULL);
  while (i < size)
    {
      s[i] = NULL;
      i++;
    }
  return (s);
}

int	my_strstrlen(char **s)
{
  int	result;

  result = 0;
  while (s[result] != NULL)
    result++;
  return (result);
}

void	my_putendl(char const *s)
{
  my_puterror(s);
  my_putchar('\n');
}

int		my_strcmp(const char *s1, const char *s2)
{
  int		i;
  unsigned char	*p1;
  unsigned char	*p2;

  i = 0;
  p1 = (unsigned char *)s1;
  p2 = (unsigned char *)s2;
  while (p1[i] == p2[i])
    {
      if (p1[i] == '\0' && p2[i] == '\0')
	return (0);
      i++;
    }
  return (p1[i] - p2[i]);
}


int		my_strncmp(const char *s1, const char *s2, size_t n)
{
  unsigned int	i;
  unsigned char	*p1;
  unsigned char	*p2;

  i = 0;
  p1 = (unsigned char *)s1;
  p2 = (unsigned char *)s2;
  if (n == 0)
    return (0);
  while (i < n)
    {
      if (p1[i] != p2[i] || (p1[i + 1] == '\0' && p2[i + 1] == '\0'))
	return (p1[i] - p2[i]);
      i++;
    }
  return (p1[i - 1] - p2[i - 1]);
}

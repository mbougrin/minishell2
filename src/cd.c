/*
** cd.c for cd  in /home/sauvag_c/rendu/PSU/PSU_2015_minishell2
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Wed Mar 23 16:39:12 2016 clément sauvage
** Last update Thu Apr  7 22:05:01 2016 clément sauvage
*/

#include "main.h"

void	my_setenv(char *line)
{
  char	**tab;
  char	*getenv;

  tab = my_str_to_word_tab(line, ' ');
  if (tab[1] == NULL)
    {
      my_strstrdel(tab);
      return;
    }
  getenv = my_getenv(tab[1]);
  if (getenv != NULL)
    my_setenv_replace(tab);
  else
    my_setenv_add(tab);
  if (getenv != NULL)
    my_strdel(&getenv);
  my_strstrdel(tab);
}

char	*my_cmd(char *s1, char *s2)
{
  char	*cmd;

  cmd = my_strnew(my_strlen(s1) + my_strlen(s2) + 9);
  my_strcat(cmd, "setenv ");
  my_strcat(cmd, s1);
  my_strcat(cmd, " ");
  my_strcat(cmd, s2);
  return (cmd);
}

void	my_swap_pwd()
{
  char	*pwd;
  char	*oldpwd;
  char	*cmd;

  pwd = my_getenv("PWD");
  oldpwd = my_getenv("OLDPWD");
  chdir(oldpwd);
  cmd = my_cmd("PWD", oldpwd);
  my_setenv(cmd);
  my_strdel(&cmd);
  cmd = my_cmd("OLDPWD", pwd);
  my_setenv(cmd);
  my_strdel(&pwd);
  my_strdel(&oldpwd);
  my_strdel(&cmd);
}

void	my_cd(char *path)
{
  char	tmp[256];
  char	*pwd;
  char	*cmd;

  pwd = my_getenv("PWD");
  cmd = my_cmd("OLDPWD", pwd);
  my_setenv(cmd);
  my_strdel(&cmd);
  chdir(path);
  getcwd(tmp, sizeof(tmp));
  cmd = my_cmd("PWD", tmp);
  my_setenv(cmd);
  my_strdel(&cmd);
  my_strdel(&pwd);
  my_bzero(tmp, 256);
}

void	my_change_directory(char **tab)
{
  if (tab[1] == NULL)
    my_cd_home();
  else if (my_strcmp(tab[1], "-") == 0)
    my_swap_pwd();
  else
    my_cd(tab[1]);
}

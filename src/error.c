/*
** error.c for error in /home/sauvag_c/rendu/PSU/PSU_2015_minishell2
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Wed Mar 23 16:39:48 2016 clément sauvage
** Last update Wed Mar 23 16:39:50 2016 clément sauvage
*/

#include "main.h"

void	my_error(char c)
{
  write(2, &c, 1);
}

void	my_puterror(const char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      my_error(str[i]);
      i++;
    }
}

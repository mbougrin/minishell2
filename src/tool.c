/*
** tool.c for tool in /home/sauvag_c/rendu/PSU/PSU_2015_minishell1/src
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Sat Jan 23 18:03:20 2016 clément sauvage
** Last update Sun Jan 24 11:55:36 2016 clément sauvage
*/

#include "main.h"

void	my_putchar(char c)
{
  write(1, &c, 1);
}

void	my_putstr(const char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      my_putchar(str[i]);
      i++;
    }
}

int	my_strlen(const char *str)
{
  int	i;

  i = 0;
  while (str[i])
    i++;
  return (i);
}

char	*my_strcpy(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s2[i] != '\0')
    {
      s1[i] = s2[i];
      i++;
    }
  s1[i] = '\0';
  return (s1);
}

char	*my_strdup(char *s1)
{
  char	*dest;
  int	len;

  len = my_strlen(s1) + 1;
  dest = (char *)malloc(sizeof(char) *(len));
  if (dest == NULL)
    return (NULL);
  my_strcpy(dest, s1);
  return (dest);
}

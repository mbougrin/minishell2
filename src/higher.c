/*
** higher.c for higher in /home/sauvag_c/liste/r
** 
** Made by clement sauvage
** Login   <sauvag_c@epitech.eu>
** 
** Started on  Fri May 22 14:10:39 2015 clement sauvage
** Last update Fri Apr  8 20:09:03 2016 cl��ment sauvage
*/

#include <redir.h>
#include "main.h"

void	my_execve(char **tbin, char *path, char **environ)
{
  pid_t	pid;
  char	*tmp;
  char	*cmd;

  tmp = my_strjoin(path, "/");
  cmd = my_strjoin(tmp, tbin[0]);
  free(tmp);
  if ((pid = fork()) < 0)
    {
      write(2, "fork error\n", 11);
      exit(-1);
    }
  if (pid == 0)
    my_execve(cmd, tbin, environ);
  else
    wait(NULL);
  free(cmd);
}

void			launch_command(char *bin)
{
  extern char 		**environ;
  char			*path;
  char			**split;
  char			**tbin;
  int			i;
  DIR			*dir;
  struct dirent		*dirent;

  i = 0;
  path = my_getenv("PATH");
  split = strsplit(path, ':');
  tbin = strsplit(bin, ' ');
  while ((dir = opendir(split[i])))
    {
      while ((dirent = readdir(dir)))
	{
	  if (my_strcmp(tbin[0], dirent->d_name) == 0)
	    {
	      my_execve(tbin, split[i], environ);
	      my_strstrdel(split);
	      my_strstrdel(tbin);
	      free(path);
	      return;
	    }
	}
      closedir(dir);
      i++;
    }
  my_strstrdel(split);
  my_strstrdel(tbin);
  free(path);

}

void	higher(char *bin, char *name)
{
  int	fd;
  int	fd_one;

  if (check_binary(bin) == -1)
    return;
  if ((fd = open(name, O_RDWR | O_CREAT | O_TRUNC, 0644)) == -1)
    {
      write(1, "Open error\n", 11);
      return;
    }
  else
    {
      fd_one = dup(1);
      dup2(fd, 1);
      launch_command(bin);
      close(fd);
      dup2(fd_one, 1);
    }
}

void	higher_higher(char *bin, char *name)
{
  int	fd;
  int	fd_one;

  if (check_binary(bin) == -1)
    return;
  if ((fd = open(name, O_RDWR | O_CREAT | O_APPEND, 0644)) == -1)
    {
      write(1, "Open error\n", 11);
      return;
    }
  else
    {
      fd_one = dup(1);
      dup2(fd, 1);
      launch_command(bin);
      close(fd);
      dup2(fd_one, 1);
    }
}

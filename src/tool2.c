/*
** tool2.c for tool2 in /home/sauvag_c/rendu/PSU/PSU_2015_minishell1/src
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Sat Jan 23 18:04:31 2016 clément sauvage
** Last update Sat Jan 23 18:39:13 2016 clément sauvage
*/

#include "main.h"
#include <string.h>
#include <stdlib.h>

char	*my_strchr(const char *s, int c)
{
  unsigned char	charact;

  charact = (unsigned char)c;
  while (*s != '\0')
    {
      if (charact == *s)
	return ((char *)s);
      s++;
    }
  if (charact == *s)
    return ((char *)s);
  return (NULL);
}

void	my_strdel(char **as)
{
  free(*as);
  *as = NULL;
}

char	*my_strjoin(char const *s1, char const *s2)
{
  char	*dst;
  int	i;
  int	j;

  i = 0;
  j = 0;
  dst = (char *)malloc(sizeof(char) * ((my_strlen(s1) + my_strlen(s2)) +1));
  if (dst == NULL)
    return (NULL);
  while (s1[i] != '\0')
    {
      dst[i] = s1[i];
      i++;
    }
  while (s2[j] != '\0')
    {
      dst[i] = s2[j];
      i++;
      j++;
    }
  dst[i] = '\0';
  return (dst);
}

char		*my_strnew(size_t size)
{
  size_t	i;
  char		*str;

  i = 0;
  str = (char *)malloc(sizeof(char) * (size + 1));
  if (str == NULL)
    return (NULL);
  while (size > i)
    {
      str[i] = '\0';
      i++;
    }
  return (str);
}

char	*my_strsub(char const *s, unsigned int start, size_t len)
{
  char	*dst;
  int	i;

  i = 0;
  dst = my_strnew(len + 1);
  if (dst == NULL)
    return (NULL);
  while (len > 0)
    {
      dst[i] = s[start];
      start++;
      i++;
      len--;
    }
  dst[i] = '\0';
  return (dst);
}

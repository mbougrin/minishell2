/*
** cd_err.c for cd_err in /home/sauvag_c/rendu/PSU/PSU_2015_minishell2
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Wed Mar 23 16:39:24 2016 clément sauvage
** Last update Thu Apr  7 22:05:17 2016 clément sauvage
*/

#include "main.h"

static void	my_cd_permission(char *s)
{
  my_putstr_fd("mysh: cd: permission denied: ", 2);
  my_putendl_fd(s, 2);
}

static void	my_cd_not_found(char *s)
{
  my_putstr_fd("mysh: cd: no such file or directory: ", 2);
  my_putendl_fd(s, 2);
}

static void	my_cd_not_directory(char *s)
{
  my_putstr_fd("mysh: cd: not a directory: ", 2);
  my_putendl_fd(s, 2);
}

int	my_cd_check(char *s)
{
  struct stat	s_stat;

  stat(s, &s_stat);
  if (access(s, F_OK) != 0)
    my_cd_not_found(s);
  else if (S_ISDIR(s_stat.st_mode) == 1)
    {
      if (access(s, F_OK | X_OK | R_OK) == 0)
	return (0);
      else
	my_cd_permission(s);
    }
  else
    my_cd_not_directory(s);
  return (1);
}

void	my_cd_home()
{
  char	*home;
  char	*pwd;
  char	*cmd;

  pwd = my_getenv("PWD");
  home = my_getenv("HOME");
  cmd = my_cmd("PWD", home);
  my_setenv(cmd);
  my_strdel(&cmd);
  cmd = my_cmd("OLDPWD", pwd);
  my_setenv(cmd);
  my_strdel(&cmd);
  chdir(home);
  my_strdel(&home);
}

void	my_bzero(void *s, size_t n)
{
  char*ptr;

  ptr = (char *)s;
  if (n == 0)
    return;
  while (n > 0)
    {
      *ptr = '\0';
      ptr++;
      n--;
    }
}

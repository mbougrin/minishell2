/*
** main.c for main.c in /home/sauvag_c/rendu/PSU/PSU_2015_minishell1/src
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Wed Jan 20 16:02:32 2016 clément sauvage
** Last update Fri Apr  8 20:26:13 2016 clément sauvage
*/

#include "main.h"

/* void	my_check_loop2(char *bin, char *name) */
/* { */
/*   if (my_strcmp(tab[0], ">") == 0) */
/*    lower(bin, name); */
/*  else if (my_strcmp(tab[0], ">>") == 0) */
/*    lower_lower(bin, name); */
/*  else if (my_strcmp(tab[0], "<<") == 0) */
/*    higher_higher(bin, name); */
/*  else if (my_strcmp(tab[0], "<") == 0) */
/*    higher(bin, name); */
/* } */

void	my_trim(char **line)
{
	int	i;

	i = 0;
	while (line[0][i] != '\0')
	{
		if (line[0][i] == '\t')
			line[0][i] = ' ';
		i++;
	}
}

void	my_copy_trim(char **line, char *home)
{
	char	*str;
	int	i;
	int	j;
	int	k;
	short   check;

	i = 0;
	k = 0;
	j = 0;
	check = 0;
	str = (char *)malloc(sizeof(char) * (my_strlen(home) +\
	my_strlen(line[0])));
	while (line[i] != '\0')
	{
		if (line[0][i] == '~')
		{
			while (home[j] != '\0')
			{
				str[k] = home[j];
				j++;
				k++;
			}
			check++;
		}
		else
		{
			str[k] = line[0][i];
			k++;
		}
		i++;
	}
	if (check > 0)
	{
		free(line[0]);
		line[0] = str;
	}
	else
		free(str);
}

void	my_trim_tilde(char **line)
{
	char	*home;
	int	i;

	home = my_getenv("HOME");
	while (line[0][i] != '\0')
	{
		if (line[0][i] == '~')
			my_copy_trim(line, home);
		i++;
	}
	free(home);
}

int	my_check_operator(char **tab)
{
	int	i;
	int	j;

	i = 0;
	while (tab[i])
	{
		j = 0;
		while (tab[i][j])
		{
			if (tab[i][j] == '<' || \
				tab[i][j] == '>' || \
				tab[i][j] == '|')
				return (0);
			j++;
		}
		i++;
	}
	return (-1);
}

char 	*my_split_shell(char *line, int choice)
{
	int	i;
	int	j;
	char	*ret;
	int	count;
	
	count = 0;
	i = 0;
	j = 0;
	ret = (char *)malloc(sizeof(char) * my_strlen(line));
	while (line[i] != '\0')
	{
		if (choice == 0)
		{
			if (line[i] == '<' || line[i] == '>' ||\
			line[i] == '|')
				break ;
			ret[j] = line[i];	
			j++;
		}
		else
		{
			if (line[i] == '<' || line[i] == '>'|| \
			line[i] == '|')
				count = 1;
			if (count == 1)
			{
				while (line[i] == '<' || line[i] == '>'|| \
				line[i] == '|')
					i++;
				ret[j] = line[i];	
				j++;
			}
		}
		i++;
	}
	ret[j] = '\0';
	return (ret);
}

void	my_operator(char *line)
{
	char	*first;
	char	*next;

	first = my_split_shell(line, 0);
	next = my_split_shell(line, 1);
	my_putstr(first);
	my_putstr(" ");
	my_putstr(next);

	
}


void	my_check_loop(char *line/*, char *bin, char *name*/)
{
  char	**tab;
  char	**tab_sep;
	int	i;


// if pipe ou flux redirection casse toi ici TODO
i=0;
	my_trim_tilde(&line);
  tab_sep = my_str_to_word_tab(line, ';');
while (tab_sep[i] != NULL)
{
	line = tab_sep[i];
	my_trim(&line);
  if (line == NULL || line[0] == '\0')
    return ;
  tab = my_str_to_word_tab(line, ' ');
  if (tab == NULL || !tab[0])
    return;
	if (my_check_operator(tab) == 0)
		my_operator(line);	
  else if (my_strcmp(tab[0], "env") == 0)
    my_env();
  else if (my_strcmp(tab[0], "cd") == 0)
    my_change_directory(tab);
  else if (my_strcmp(tab[0], "setenv") == 0)
    my_setenv(line);
  else if (my_strcmp(tab[0], "unsetenv") == 0)
    my_unsetenv(line);
  //  my_check_loop2(bin, name);
else if (tab[0][0] == '/' /* TODO CONDITION CHECK EXIST BINARY*/)
	my_execve(tab[0], tab);
else if (tab[0][0] == '.' && tab[0][1] == '/')
	my_execve(&tab[0][2], tab);
  else
    my_launch_binary(line);
  my_strstrdel(tab);
i++;
}
  my_strstrdel(tab_sep);
}

void	my_loop(void)
{
  char	*line;

  while (1)
    {
      my_putstr("$> ");
      get_next_line(0, &line);
      if (my_strcmp(line, "exit") == 0)
	{
	  //	  my_putendl(line);
	  my_strdel(&line);
	  break;
	}
      my_check_loop(line);
      my_strdel(&line);
    }
}

int	main(int ac, char **av, char **env)
{
  (void)av;
  if (ac == 1)
    {
      my_signal();
      my_envp(my_strstrdup(env));
      my_loop();
      my_strstrdel(my_envp(NULL));
    }
  return (0);
}

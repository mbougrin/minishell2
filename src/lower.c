/*
** lower.c for lower in /home/sauvag_c/liste/r
** 
** Made by clement sauvage
** Login   <sauvag_c@epitech.eu>
** 
** Started on  Fri May 22 16:18:27 2015 clement sauvage
** Last update Thu Apr  7 22:07:19 2016 clément sauvage
*/
#include "redir.h"

void	read_fd(char *name)
{
  int	fd;
  int	ret;
  char	line[4096];

  if ((fd = open(name, O_RDONLY)) == -1)
    {
      write(2, "open error\n", 11);
      exit(-1);
    }
  while ((ret = read(fd, line, 4096)))
    {
      line[ret] = '\0';
      my_putstr(line);
    }
}

int	my_tempnam(char *end)
{
  int	fd;
  char	line[4096];
  int	i;
  int	ret;

  fd = open("/tmp/file_tmp", O_RDWR | O_CREAT | O_TRUNC, 0644);
  if (fd == -1)
    exit(-1);
  while (1)
    {
      my_putstr(">");
      if ((ret = read(0, line, 4096)) == 0)
	{
	  write(1, "\n", 1);
	  exit(-1);
	}
      line[ret] = '\0';
      if (my_strncmp(line, end, my_strlen(end)) == 0)
	break;
      write(fd, line, my_strlen(line));
      i = 0;
      while (line[i] != '\0')
	{
	  line[i] = '\0';
	  i++;
	}
    }
  sig_int  close(fd);
  return (open("/tmp/file_tmp", O_RDONLY));
}

void	read_lower_lower(int fd)
{
  int	ret;
  char	line[4096];

  while ((ret = read(fd, line, 4096)))
    {
      line[ret] = '\0';
      my_putstr(line);
    }
}

void	lower(char *bin, char *name)
{
  int	fd;
  int	fd_zero;

  if (bin != NULL && check_binary(bin) == -1)
    return;
  if (bin == NULL)
    {
      read_fd(name);
      return;
    }
  if ((fd = open(name, O_RDONLY)) == -1)
    write(2, "open error\n", 11);
  else
    {
      fd_zero = dup(0);
      dup2(fd, 0);
      launch_command(bin);
      close(fd);
      dup2(fd_zero, 0);
    }
}

void	lower_lower(char *bin, char *name)
{
  int	fd;
  int	fd_zero;

  if (bin != NULL && check_binary(bin) == -1)
    return;
  fd = my_tempnam(name);
  if (bin == NULL)
    {
      read_lower_lower(fd);
      close(fd);
      return;
    }
  fd_zero = dup(0);
  dup2(fd, 0);
  launch_command(bin);
  close(fd);
  dup2(fd_zero, 0);
}

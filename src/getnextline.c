/*
** getnextline.c for getnextline in /home/sauvag_c/rendu/PSU/PSU_2015_minishell2
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Wed Mar 23 16:40:16 2016 clément sauvage
** Last update Wed Mar 23 16:40:17 2016 clément sauvage
*/

#include "main.h"

static char	*my_copy(char *buff_tmp, char **line)
{
  int		i;
  int		j;
  char		*tmp;

  i = 0;
  while (buff_tmp[i] != '\n')
    i++;
  j = i;
  while (buff_tmp[j] != '\0')
    j++;
  buff_tmp[i] = '\0';
  *line = my_strdup(buff_tmp);
  buff_tmp[i] = '\n';
  i++;
  if (buff_tmp[i] != '\0')
    {
      tmp = buff_tmp;
      buff_tmp = my_strsub(tmp, i, j);
      my_strdel(&tmp);
      return (buff_tmp);
    }
  my_strdel(&buff_tmp);
  return (buff_tmp);
}

static char	*my_copy_buff_tmp(char *buff_tmp, char *buff)
{
  char		*tmp;

  if (buff_tmp == NULL)
    buff_tmp = my_strdup(buff);
  else
    {
      tmp = buff_tmp;
      buff_tmp = my_strjoin(tmp, buff);
      my_strdel(&tmp);
    }
  my_strdel(&buff);
  return (buff_tmp);
}

int		get_next_line(int fd, char **line)
{
  static char	*buff_tmp;
  char		*buff;
  int		ret;

  if (buff_tmp != NULL && my_strchr(buff_tmp, '\n') != NULL)
    {
      buff_tmp = my_copy(buff_tmp, line);
      return (1);
    }
  buff = (char *)malloc(sizeof(char) * (BUFF_SIZE + 1));
  while ((ret = read(fd, buff, BUFF_SIZE)))
    {
      if (ret == -1 || fd == -1)
	return (-1);
      buff[ret] = '\0';
      if (my_strchr(buff, '\n') != NULL)
	{
	  buff_tmp = my_copy_buff_tmp(buff_tmp, buff);
	  buff_tmp = my_copy(buff_tmp, line);
	  return (1);
	}
      buff_tmp = my_copy_buff_tmp(buff_tmp, buff);
      buff = (char *)malloc(sizeof(char) * (BUFF_SIZE + 1));
    }
  return (0);
}

/*
** tool5.c for tool5 in /home/sauvag_c/rendu/PSU/PSU_2015_minishell1/src
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Sat Jan 23 18:29:21 2016 clément sauvage
** Last update Thu Apr  7 13:23:03 2016 clément sauvage
*/

#include "main.h"
#include <stdlib.h>
#include <unistd.h>

void	my_putchar_fd(char c, int fd)
{
  write(fd, &c, 1);
}

void	my_putstr_fd(char const *s, int fd)
{
  while (*s != '\0')
    {
      my_putchar_fd(*s, fd);
      s++;
    }
}

void	my_putendl_fd(char const *s, int fd)
{
  my_putstr_fd(s, fd);
  my_putchar_fd('\n', fd);
}

char	**my_strstrdup(char **s)
{
  char	**str;
  int	i;

  i = 0;
  str = my_strstrnew(my_strstrlen(s) + 1);
  while (s[i] != NULL)
    {
      str[i] = my_strdup(s[i]);
      i++;
    }
  str[i] = NULL;
  return (str);
}

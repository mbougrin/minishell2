/*
** env.c for env in /home/sauvag_c/rendu/PSU/PSU_2015_minishell2
** 
** Made by clément sauvage
** Login   <sauvag_c@epitech.net>
** 
** Started on  Wed Mar 23 16:39:36 2016 clément sauvage
** Last update Thu Apr  7 22:05:36 2016 clément sauvage
*/

#include "main.h"

void	my_del_env(char **env, char **tab, char **env_tmp)
{
  int	j;
  int	i;
  int	ret;
  int	count;

  i = 0;
  count = 0;
  while (env[i])
    {
      j = 1;
      ret = 0;
      while (tab[j])
	{
	  if (my_strstr(env[i], tab[j]))
	    ret = 1;
	  j++;
	}
      if (ret == 0)
	{
	  env_tmp[count] = my_strdup(env[i]);
	  count++;
	}
      i++;
    }
  my_strstrdel(env);
  my_envp(env_tmp);
}

void	my_unsetenv(char *line)
{
  char	**tab;
  char	**env;
  char	**env_tmp;
  
  env = my_envp(NULL);
  tab = my_str_to_word_tab(line, ' ');
  if (tab[1] == NULL)
    {
      my_strstrdel(tab);
      return;
    }
  env_tmp = my_strstrnew(my_check_del(tab, env));
  my_del_env(env, tab, env_tmp);
  my_strstrdel(tab);
}

char	*my_setenv_join(char **tab)
{
  char	*ret;
  int	len;

  len = 0;
  if (tab[2] != NULL)
    len = my_strlen(tab[2]);
  ret = my_strnew(my_strlen(tab[1]) + len + 1);
  my_strcat(ret, tab[1]);
  my_strcat(ret, "=");
  if (tab[2] != NULL)
    my_strcat(ret, tab[2]);
  return (ret);
}

void	my_setenv_replace(char **tab)
{
  char	**env;
  int	i;
  int	len;
  int	j;

  env = my_envp(NULL);
  i = 1;
  while (tab[i])
    {
      j = 0;
      while (env[j])
	{
	  len = my_strlen(tab[i]);
	  if (my_strncmp(env[j], tab[i], len) == 0)
	    {
	      my_strdel(&env[j]);
	      env[j] = my_setenv_join(tab);
	    }
	  j++;
	}
      i++;
    }
}

void	my_setenv_add(char **tab)
{
  char	**env;
  char	**env_tmp;
  int	len;
  int	i;

  i = 0;
  env = my_envp(NULL);
  len = my_strstrlen(env);
  env_tmp = my_strstrnew(len + 1);
  while (env[i])
    {
      env_tmp[i] = my_strdup(env[i]);
      i++;
    }
  env_tmp[i] = my_setenv_join(tab); 
  env_tmp[i + 1] = NULL;
  my_strstrdel(env);
  my_envp(env_tmp);
}
